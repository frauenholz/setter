/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    Collection,
    DateTime,
    Forms,
    L10n,
    NodeBlock,
    Slots,
    Str,
    affects,
    alias,
    created,
    definition,
    deleted,
    editor,
    filter,
    getMetadata,
    icon,
    insertVariable,
    isBoolean,
    isDefined,
    isFilledString,
    isNumberFinite,
    isString,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    map,
    name,
    pgettext,
    populateVariables,
    set,
    title,
    width,
} from "tripetto";
import { ICalculatorMetadata } from "tripetto-block-calculator";
import { Setter } from ".";
import { TMode } from "../runner/mode";
import { getIcon } from "./icon";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { ICalculator, Operation, calculator } from "tripetto-block-calculator";
import "./namespace/unmount";

export class Value extends Collection.Item<Setter> implements ICalculator {
    readonly startBlank = true;

    private get setter() {
        return this.ref;
    }

    private get currentVariable() {
        return (
            (this.variable && lookupVariable(this.setter, this.variable)) ||
            undefined
        );
    }

    get block() {
        return this.setter;
    }

    @definition
    @affects("#name")
    readonly operations: Collection.Provider<Operation, ICalculator> =
        Collection.of<Operation, ICalculator>(Operation, this, true);

    @definition
    readonly variable: string | undefined;

    @definition
    @affects("#name")
    value?: string | number | boolean;

    @definition
    reference?: string;

    @definition
    @affects("#name")
    querystring?: true;

    @definition
    @affects("#name")
    lock?: boolean;

    @definition
    @affects("#name")
    mode?: TMode;

    @definition
    @affects("#name")
    separator?: string;

    @icon
    get icon() {
        return getIcon(this.currentVariable?.slot || undefined);
    }

    @name
    get name() {
        const variable = this.currentVariable;

        if (variable) {
            if (
                (variable.slot instanceof Slots.String ||
                    variable.slot instanceof Slots.Text) &&
                isString(this.value)
            ) {
                return `@${variable.key} ${
                    this.mode === "concatenate"
                        ? `+ ${(this.separator || "")
                              .replace("\n\n", "¶")
                              .replace("\n", "↵")}`
                        : "= "
                }${
                    !this.querystring && isVariable(this.value)
                        ? (lookupVariable(this.setter, this.value)?.label &&
                              `@${this.value}`) ||
                          ""
                        : `${this.querystring ? "`?" : ""}${
                              makeMarkdownSafe(
                                  Str.replace(this.value, "\n", "↵")
                              ) || (this.querystring ? "..." : "\\_\\_")
                          }${this.querystring ? "=...`" : ""}`
                }`;
            } else if (
                (variable.slot instanceof Slots.Number ||
                    variable.slot instanceof Slots.Numeric) &&
                (this.value === true ||
                    isNumberFinite(this.value) ||
                    isString(this.value))
            ) {
                return `@${variable.key} ${
                    this.mode === "+" || this.mode === "-"
                        ? this.mode
                        : this.mode === "*"
                        ? "×"
                        : this.mode === "/"
                        ? "÷"
                        : "="
                } ${
                    this.querystring
                        ? `\`?${
                              (isFilledString(this.value) &&
                                  makeMarkdownSafe(this.value)) ||
                              "..."
                          }=...\``
                        : this.value === true
                        ? "`" +
                          pgettext(
                              "block:setter",
                              "Calculation outcome"
                          ).toUpperCase() +
                          "`"
                        : isFilledString(this.value)
                        ? (lookupVariable(this.setter, this.value)?.label &&
                              `@${this.value}`) ||
                          ""
                        : `**${variable.slot.toString(
                              this.value,
                              (n: number | string, p?: number | "auto") =>
                                  L10n.locale.number(n, p, false)
                          )}**`
                }`;
            } else if (
                variable.slot instanceof Slots.Boolean &&
                (isBoolean(this.value) || isString(this.value))
            ) {
                return `@${variable.key} ${
                    this.mode === "OR" ||
                    this.mode === "NOR" ||
                    this.mode === "AND" ||
                    this.mode === "NAND" ||
                    this.mode === "XOR" ||
                    this.mode === "XNOR"
                        ? this.mode
                        : "="
                } ${
                    this.querystring
                        ? `\`?${
                              (isFilledString(this.value) &&
                                  makeMarkdownSafe(this.value)) ||
                              "..."
                          }=...\``
                        : isFilledString(this.value)
                        ? (lookupVariable(this.setter, this.value)?.label &&
                              `@${this.value}`) ||
                          ""
                        : `**${makeMarkdownSafe(
                              variable.slot.toString(this.value)
                          )}**`
                }`;
            } else if (
                variable.slot instanceof Slots.Date &&
                (this.value === true ||
                    isNumberFinite(this.value) ||
                    isString(this.value))
            ) {
                return `@${variable.key} = ${
                    this.querystring
                        ? `\`?${
                              (isFilledString(this.value) &&
                                  makeMarkdownSafe(this.value)) ||
                              "..."
                          }=...\``
                        : this.value === true
                        ? "`" +
                          pgettext("block:setter", "Now").toUpperCase() +
                          "`"
                        : isFilledString(this.value)
                        ? (lookupVariable(this.setter, this.value)?.label &&
                              `@${this.value}`) ||
                          ""
                        : `**${
                              variable.slot.supportsTime
                                  ? L10n.locale.dateTimeShort(
                                        variable.slot.toValue(this.value),
                                        true
                                    )
                                  : L10n.locale.dateShort(
                                        variable.slot.toValue(this.value),
                                        true
                                    )
                          }**`
                }`;
            }

            return "@" + variable.key;
        }

        return "";
    }

    @title
    get title() {
        return (
            this.currentVariable?.label ||
            pgettext("block:setter", "Invalid variable")
        );
    }

    @alias
    get overwriteLabel(): string | undefined {
        const variable = this.currentVariable;

        return (
            (this.value === undefined &&
                pgettext("block:setter", "Unchanged").toUpperCase()) ||
            (this.value ===
                (variable?.slot instanceof Slots.Boolean ? 0 : false) &&
                pgettext("block:setter", "Clear").toUpperCase()) ||
            (this.mode === undefined &&
                this.setter.isAdvanced &&
                pgettext("block:setter", "Prefill").toUpperCase()) ||
            undefined
        );
    }

    @width
    get width() {
        return this.currentVariable?.slot instanceof Slots.Date ? 475 : 400;
    }

    @created
    attachVariable(id: string) {
        let variable = this.currentVariable;

        if (id) {
            set(this, "variable", id);

            variable = this.currentVariable;

            if (
                variable?.slot instanceof Slots.String ||
                variable?.slot instanceof Slots.Text
            ) {
                this.value = "";
            } else if (
                variable?.slot instanceof Slots.Number ||
                variable?.slot instanceof Slots.Numeric
            ) {
                this.value = 0;
            } else if (
                variable?.slot instanceof Slots.Boolean ||
                variable?.slot instanceof Slots.Date
            ) {
                this.value = true;
            }

            if (this.setter.isAdvanced) {
                this.mode = "overwrite";
            }

            this.open();
        }

        variable?.node?.hook(
            "OnBlockUnassign",
            "framed",
            () => this.close(),
            this
        );
        variable?.node?.hook("OnItemPop", "framed", () => this.delete(), this);

        return this;
    }

    @deleted
    detachVariable() {
        this.currentVariable?.node?.unhookContext(this);
    }

    @editor
    defineEditor(): void {
        const variable = this.currentVariable;

        if (variable && variable.block instanceof NodeBlock) {
            let suggestions: Forms.ITextSuggestion[] | undefined;
            let options: Forms.IDropdownOption<string>[] | undefined;
            const supply = Collection.find(variable);

            if (supply) {
                if (supply.sole) {
                    options = map(supply.collection.all, (suggestion) => ({
                        value: suggestion.id,
                        label: suggestion.labelWithoutMarkdown,
                    }));
                } else {
                    suggestions = map(supply.collection.all, (suggestion) => ({
                        id: suggestion.id,
                        name: suggestion.labelWithoutMarkdown,
                    }));
                }
            } else {
                const metadata = getMetadata<ICalculatorMetadata>(
                    variable.block,
                    "calculator"
                );

                if (metadata) {
                    const data =
                        metadata[variable.slot?.reference || "*"] ||
                        metadata["*"];

                    if (data && data.scores) {
                        const scores = filter(data.scores, (score) =>
                            isFilledString(score.reference) && score.label
                                ? true
                                : false
                        );

                        if (data.allowDefault === false) {
                            options = map(scores, (score) => ({
                                value: score.reference as string,
                                label: score.label as string,
                            }));
                        } else {
                            suggestions = map(scores, (score) => ({
                                id: score.reference as string,
                                name: score.label as string,
                            }));
                        }
                    }
                }
            }

            const variableValue =
                isFilledString(this.value) && isVariable(this.value)
                    ? this.value
                    : undefined;

            const variables = populateVariables(
                this.setter,
                (slot, pipe) => {
                    if (
                        variable.slot instanceof Slots.String ||
                        variable.slot instanceof Slots.Text
                    ) {
                        return true;
                    } else if (
                        variable.slot instanceof Slots.Number ||
                        variable.slot instanceof Slots.Numeric
                    ) {
                        return (
                            slot instanceof Slots.Number ||
                            slot instanceof Slots.Numeric
                        );
                    } else if (variable.slot instanceof Slots.Boolean) {
                        return !pipe && slot instanceof Slots.Boolean;
                    } else if (variable.slot instanceof Slots.Date) {
                        return slot instanceof Slots.Date;
                    }

                    return false;
                },
                variableValue,
                false,
                variable.key
            );

            const valueControl =
                variable.slot instanceof Slots.String ||
                variable.slot instanceof Slots.Text
                    ? [
                          options
                              ? new Forms.Dropdown(
                                    options,
                                    !variableValue &&
                                    !this.querystring &&
                                    isFilledString(this.reference)
                                        ? this.reference
                                        : ""
                                )
                                    .on((input) => {
                                        if (input.isObservable) {
                                            this.value =
                                                input.optionLabel(
                                                    input.value
                                                ) || "";
                                            this.reference =
                                                input.value || undefined;
                                        }
                                    })
                                    .label(
                                        pgettext(
                                            "block:setter",
                                            "Use fixed value"
                                        )
                                    )
                                    .disabled(options.length === 0)
                                    .autoFocus()
                              : new Forms.Text(
                                    suggestions ? "singleline" : "multiline",
                                    !variableValue &&
                                    !this.querystring &&
                                    isFilledString(this.value)
                                        ? this.value
                                        : ""
                                )
                                    .on((input) => {
                                        if (input.isObservable) {
                                            this.value = input.value;
                                            this.reference =
                                                input.suggestion?.id ||
                                                undefined;
                                        }
                                    })
                                    .label(
                                        pgettext(
                                            "block:setter",
                                            "Use fixed text"
                                        )
                                    )
                                    .suggestions(suggestions)
                                    .action(
                                        "@",
                                        insertVariable(this.setter, "validated")
                                    )
                                    .autoFocus()
                                    .escape(this.editor.close),
                      ]
                    : variable.slot instanceof Slots.Number ||
                      variable.slot instanceof Slots.Numeric
                    ? [
                          new Forms.Numeric(
                              isNumberFinite(this.value) ? this.value : 0
                          )
                              .label(
                                  pgettext("block:setter", "Use fixed number")
                              )
                              .precision(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.precision) ||
                                      0
                              )
                              .digits(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.digits) ||
                                      0
                              )
                              .decimalSign(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.decimal) ||
                                      ""
                              )
                              .thousands(
                                  variable.slot instanceof Slots.Numeric &&
                                      variable.slot.separator
                                      ? true
                                      : false,
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.separator) ||
                                      ""
                              )
                              .prefix(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.prefix) ||
                                      ""
                              )
                              .prefixPlural(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.prefixPlural) ||
                                      undefined
                              )
                              .suffix(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.suffix) ||
                                      ""
                              )
                              .suffixPlural(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.suffixPlural) ||
                                      undefined
                              )
                              .min(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.minimum) ||
                                      undefined
                              )
                              .max(
                                  (variable.slot instanceof Slots.Numeric &&
                                      variable.slot.maximum) ||
                                      undefined
                              )
                              .autoFocus()
                              .escape(this.editor.close)
                              .enter(this.editor.close)
                              .on((input) => {
                                  if (input.isObservable) {
                                      this.value = input.value;
                                  }
                              }),
                      ]
                    : variable.slot instanceof Slots.Date
                    ? [
                          new Forms.DateTime(
                              isNumberFinite(this.value)
                                  ? this.value
                                  : DateTime.UTCToday
                          )
                              .label(
                                  variable.slot.supportsTime
                                      ? pgettext(
                                            "block:setter",
                                            "Use fixed date/time"
                                        )
                                      : pgettext(
                                            "block:setter",
                                            "Use fixed date"
                                        )
                              )
                              .features(
                                  Forms.DateTimeFeatures.Date |
                                      (variable.slot.supportsTime
                                          ? Forms.DateTimeFeatures
                                                .TimeHoursAndMinutesOnly
                                          : Forms.DateTimeFeatures.Weekday)
                              )
                              .years(
                                  new Date().getFullYear() - 150,
                                  new Date().getFullYear() + 50
                              )
                              .zone("UTC")
                              .width("full")
                              .required()
                              .on((input) => {
                                  if (input.isObservable) {
                                      this.value = input.value;
                                  }
                              }),
                      ]
                    : [];

            const separatorControl = new Forms.Text(
                "singleline",
                isString(this.separator) &&
                this.separator !== "\n" &&
                this.separator !== "\n\n" &&
                this.separator !== ", "
                    ? this.separator
                    : ""
            )
                .sanitize(false)
                .placeholder(pgettext("block:setter", "Type separator here..."))
                .visible(
                    isString(this.separator) &&
                        this.separator !== "\n" &&
                        this.separator !== "\n\n" &&
                        this.separator !== ", "
                )
                .on((separator) => {
                    if (separator.isObservable) {
                        this.separator = separator.value;
                    }
                });
            const concatenateControl = new Forms.Group([
                new Forms.Dropdown<undefined | "\n" | "\n\n" | ", " | true>(
                    [
                        {
                            value: undefined,
                            label: pgettext("block:setter", "Nothing"),
                        },
                        {
                            value: "\n",
                            label:
                                "↵ " +
                                pgettext(
                                    "block:setter",
                                    "New line (single enter)"
                                ),
                            disabled: suggestions ? true : false,
                        },
                        {
                            value: "\n\n",
                            label:
                                "¶ " +
                                pgettext(
                                    "block:setter",
                                    "New paragraph (double enter)"
                                ),
                            disabled: suggestions ? true : false,
                        },
                        {
                            value: ", ",
                            label: pgettext("block:setter", "Comma + space"),
                            disabled: suggestions ? true : false,
                        },
                        {
                            value: true,
                            label: pgettext("block:setter", "Custom"),
                        },
                    ],
                    this.separator === "\n" ||
                    this.separator === "\n\n" ||
                    this.separator === ", "
                        ? this.separator
                        : isString(this.separator)
                        ? true
                        : undefined
                )
                    .label(
                        pgettext(
                            "block:setter",
                            "Separate concatenated values with"
                        )
                    )
                    .on((separator) => {
                        if (separator.isObservable && separator.value) {
                            if (isString(separator.value)) {
                                this.separator = separator.value;
                            }
                        } else {
                            this.separator = undefined;
                        }

                        separatorControl.visible(separator.value === true);
                    }),
                separatorControl,
            ]).visible(this.mode === "concatenate");

            const variableControl = new Forms.Dropdown(
                variables,
                variableValue || ""
            )
                .label(pgettext("block:setter", "Use value of"))
                .width("full")
                .visible(variableValue ? true : false)
                .disabled(variables.length === 0)
                .on((input) => {
                    if (input.isObservable) {
                        this.value = input.value || "";
                        this.reference = undefined;
                    }
                });

            const querystringControl = new Forms.Text(
                "singleline",
                this.querystring && isFilledString(this.value) ? this.value : ""
            )
                .on((input) => {
                    if (input.isObservable) {
                        this.value = input.value;
                        this.reference = undefined;
                    }
                })
                .label(pgettext("block:setter", "Query string parameter"))
                .autoFocus()
                .enter(this.editor.close)
                .escape(this.editor.close);

            const prefillNotification = this.editor
                .form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "block:setter",
                                "This value will only be set when there is _no_ current value!"
                            ),
                            "info"
                        ).markdown(),
                    ],
                })
                .visible(
                    this.mode === undefined &&
                        this.value !== undefined &&
                        this.value !==
                            (variable.slot instanceof Slots.Boolean ? 0 : false)
                );

            this.editor.form({
                title: pgettext("block:setter", "Value"),
                controls: [
                    new Forms.Radiobutton<
                        | undefined
                        | "value"
                        | "variable"
                        | "querystring"
                        | "auto"
                        | "clear"
                        | boolean
                    >(
                        [
                            {
                                label:
                                    "**" +
                                    pgettext("block:setter", "Do not change") +
                                    "**",
                                value: undefined,
                                markdown: true,
                                visible: this.setter.isAdvanced,
                            },
                            ...(variable.slot instanceof Slots.String ||
                            variable.slot instanceof Slots.Text
                                ? [
                                      {
                                          label: options
                                              ? pgettext(
                                                    "block:setter",
                                                    "Fixed value"
                                                )
                                              : pgettext(
                                                    "block:setter",
                                                    "Fixed text"
                                                ),
                                          value: "value" as "value",
                                          disabled:
                                              options && options.length === 0,
                                      },
                                  ]
                                : variable.slot instanceof Slots.Number ||
                                  variable.slot instanceof Slots.Numeric
                                ? [
                                      {
                                          label: pgettext(
                                              "block:setter",
                                              "Fixed number"
                                          ),
                                          value: "value" as "value",
                                      },
                                  ]
                                : variable.slot instanceof Slots.Boolean
                                ? [
                                      {
                                          label:
                                              variable.slot.labelForTrue ||
                                              pgettext(
                                                  "block:setter",
                                                  "Set to %1",
                                                  "**True**"
                                              ),
                                          value: true,
                                          markdown: !variable.slot.labelForTrue,
                                      },
                                      {
                                          label:
                                              variable.slot.labelForFalse ||
                                              pgettext(
                                                  "block:setter",
                                                  "Set to %1",
                                                  "**False**"
                                              ),
                                          value: false,
                                          markdown:
                                              !variable.slot.labelForFalse,
                                      },
                                  ]
                                : variable.slot instanceof Slots.Date
                                ? [
                                      {
                                          label: variable.slot.supportsTime
                                              ? pgettext(
                                                    "block:setter",
                                                    "Current date/time"
                                                )
                                              : pgettext(
                                                    "block:setter",
                                                    "Current date"
                                                ),
                                          value: "auto" as "auto",
                                      },
                                      {
                                          label: variable.slot.supportsTime
                                              ? pgettext(
                                                    "block:setter",
                                                    "Fixed date/time"
                                                )
                                              : pgettext(
                                                    "block:setter",
                                                    "Fixed date"
                                                ),
                                          value: "value" as "value",
                                      },
                                  ]
                                : []),
                            {
                                label: pgettext(
                                    "block:setter",
                                    "From other variable"
                                ),
                                value: "variable",
                                disabled: variables.length === 0,
                            },
                            {
                                label: pgettext(
                                    "block:setter",
                                    "From query string"
                                ),
                                value: "querystring",
                            },
                            ...(variable.slot instanceof Slots.Number ||
                            variable.slot instanceof Slots.Numeric
                                ? [
                                      {
                                          label: pgettext(
                                              "block:setter",
                                              "Calculation"
                                          ),
                                          value: "auto" as "auto",
                                      },
                                  ]
                                : []),
                            {
                                label: pgettext("block:setter", "Clear"),
                                value: "clear",
                                visible: this.setter.isAdvanced,
                            },
                        ],
                        this.querystring
                            ? "querystring"
                            : this.value ===
                              (variable.slot instanceof Slots.Boolean
                                  ? 0
                                  : false)
                            ? "clear"
                            : variableValue
                            ? "variable"
                            : (variable.slot instanceof Slots.Number ||
                                  variable.slot instanceof Slots.Numeric ||
                                  variable.slot instanceof Slots.Date) &&
                              this.value === true
                            ? "auto"
                            : this.value === true || this.value === false
                            ? this.value
                            : isDefined(this.value)
                            ? "value"
                            : undefined
                    ).on((type) => {
                        if (valueControl.length === 1) {
                            valueControl[0].visible(type.value === "value");
                        }

                        variableControl.visible(type.value === "variable");
                        querystringControl.visible(
                            type.value === "querystring"
                        );
                        calculatorFeature.visible(
                            (variable.slot instanceof Slots.Number ||
                                variable.slot instanceof Slots.Numeric) &&
                                type.value === "auto"
                        );
                        modeFeature.visible(
                            this.setter.isAdvanced &&
                                type.value !== undefined &&
                                type.value !== "clear"
                        );
                        concatenateControl.visible(
                            modeFeature.isObservable &&
                                this.mode === "concatenate"
                        );

                        if (
                            type.value === false ||
                            type.value === true ||
                            type.value === undefined
                        ) {
                            this.value = type.value;
                            this.reference = undefined;
                        } else if (type.value === "auto") {
                            this.value = true;
                            this.reference = undefined;
                        } else if (type.value === "clear") {
                            this.value =
                                variable.slot instanceof Slots.Boolean
                                    ? 0
                                    : false;
                            this.reference = undefined;
                        }

                        this.querystring =
                            type.value === "querystring" ? true : undefined;

                        this.setter.verifyPurpose();
                    }),
                    concatenateControl,
                    ...valueControl,
                    variableControl,
                    querystringControl,
                ],
            });

            const calculatorFeature = this.editor
                .collection(
                    calculator(
                        this,
                        undefined,
                        pgettext("block:setter", "Calculator")
                    )
                )
                .visible(
                    (variable.slot instanceof Slots.Number ||
                        variable.slot instanceof Slots.Numeric) &&
                        this.value === true
                );

            const modeFeature = this.editor
                .form({
                    title: pgettext("block:setter", "Mode"),
                    controls: [
                        new Forms.Radiobutton<TMode | undefined>(
                            [
                                {
                                    label:
                                        "**" +
                                        pgettext("block:setter", "Overwrite") +
                                        "**",
                                    description: pgettext(
                                        "block:setter",
                                        "Always overwrite the current value."
                                    ),
                                    markdown: true,
                                    value: "overwrite",
                                },
                                ...(!options &&
                                (variable.slot instanceof Slots.String ||
                                    variable.slot instanceof Slots.Text)
                                    ? [
                                          {
                                              label: pgettext(
                                                  "block:setter",
                                                  "Concatenate"
                                              ),
                                              description: pgettext(
                                                  "block:setter",
                                                  "Appends text to the current value."
                                              ),
                                              value: "concatenate" as "concatenate",
                                          },
                                      ]
                                    : []),
                                ...(variable.slot instanceof Slots.Number ||
                                variable.slot instanceof Slots.Numeric
                                    ? [
                                          {
                                              label:
                                                  pgettext(
                                                      "block:setter",
                                                      "Add"
                                                  ) + " **+**",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Add to the current value."
                                              ),
                                              value: "+" as "+",
                                              markdown: true,
                                          },
                                          {
                                              label:
                                                  pgettext(
                                                      "block:setter",
                                                      "Subtract"
                                                  ) + " **-**",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Subtract from the current value."
                                              ),
                                              value: "-" as "-",
                                              markdown: true,
                                          },
                                          {
                                              label:
                                                  pgettext(
                                                      "block:setter",
                                                      "Multiply"
                                                  ) + " **×**",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Multiply the current value with."
                                              ),
                                              value: "*" as "*",
                                              markdown: true,
                                          },
                                          {
                                              label:
                                                  pgettext(
                                                      "block:setter",
                                                      "Divide"
                                                  ) + " **÷**",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Divide the current value by."
                                              ),
                                              value: "/" as "/",
                                              markdown: true,
                                          },
                                      ]
                                    : []),
                                ...(variable.slot instanceof Slots.Boolean
                                    ? [
                                          {
                                              label: "OR [?](https://en.wikipedia.org/wiki/OR_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value and/or current value is true."
                                              ),
                                              markdown: true,
                                              value: "OR" as "OR",
                                          },
                                          {
                                              label: "NOR [?](https://en.wikipedia.org/wiki/NOR_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value and current value are both false."
                                              ),
                                              markdown: true,
                                              value: "NOR" as "NOR",
                                          },
                                          {
                                              label: "AND [?](https://en.wikipedia.org/wiki/AND_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value and current value are both true."
                                              ),
                                              markdown: true,
                                              value: "AND" as "AND",
                                          },
                                          {
                                              label: "NAND [?](https://en.wikipedia.org/wiki/NAND_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value and current value are not both true."
                                              ),
                                              markdown: true,
                                              value: "NAND" as "NAND",
                                          },
                                          {
                                              label: "XOR [?](https://en.wikipedia.org/wiki/XOR_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value is not equal to the current value."
                                              ),
                                              markdown: true,
                                              value: "XOR" as "XOR",
                                          },
                                          {
                                              label: "XNOR [?](https://en.wikipedia.org/wiki/XNOR_gate)",
                                              description: pgettext(
                                                  "block:setter",
                                                  "Set value is equal to the current value."
                                              ),
                                              markdown: true,
                                              value: "XNOR" as "XNOR",
                                          },
                                      ]
                                    : []),
                                {
                                    label: pgettext("block:setter", "Prefill"),
                                    description: pgettext(
                                        "block:setter",
                                        "Only set when there is no value yet."
                                    ),
                                    value: undefined,
                                },
                            ],
                            Forms.Radiobutton.bind(this, "mode", undefined)
                        ).on((mode) => {
                            prefillNotification.visible(
                                mode.value === undefined &&
                                    this.value !== undefined &&
                                    this.value !==
                                        (variable.slot instanceof Slots.Boolean
                                            ? 0
                                            : false)
                            );

                            concatenateControl.visible(
                                mode.isObservable &&
                                    mode.value === "concatenate"
                            );

                            this.setter.verifyPurpose();
                        }),
                    ],
                })
                .visible(
                    (this.setter.isAdvanced &&
                        isDefined(this.value) &&
                        this.value !==
                            (variable.slot instanceof Slots.Boolean
                                ? 0
                                : false)) ||
                        false
                );

            this.editor
                .form({
                    title: pgettext("block:setter", "Locking"),
                    controls: [
                        new Forms.Radiobutton<undefined | boolean>(
                            [
                                {
                                    label:
                                        "**" +
                                        pgettext(
                                            "block:setter",
                                            "Do not change"
                                        ) +
                                        "**",
                                    description: pgettext(
                                        "block:setter",
                                        "Do not lock or unlock the variable."
                                    ),
                                    markdown: true,
                                    value: undefined,
                                },
                                {
                                    label: pgettext("block:setter", "Lock"),
                                    description: pgettext(
                                        "block:setter",
                                        "Prevent further changes to the variable. If the variable is a visible block, it will become readonly."
                                    ),
                                    value: true,
                                },
                                {
                                    label: pgettext("block:setter", "Unlock"),
                                    description: pgettext(
                                        "block:setter",
                                        "Remove a lock and allow changes to the variable again."
                                    ),
                                    value: false,
                                },
                            ],
                            Forms.Radiobutton.bind(this, "lock", undefined)
                        ).on(() => this.setter.verifyPurpose()),
                        new Forms.Static(
                            pgettext(
                                "block:setter",
                                "When a variable is locked its value can only be changed using the **%1** block.",
                                Setter.label
                            )
                        ).markdown(),
                    ],
                })
                .visible(this.setter.isAdvanced);
        }
    }
}
