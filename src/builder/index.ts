/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    Str,
    affects,
    definition,
    editor,
    findFirst,
    isBoolean,
    isFilledString,
    lookupVariable,
    npgettext,
    pgettext,
    tripetto,
} from "tripetto";
import { Value } from "./value";
import { variablesMenu } from "./menu";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    kind: "headless",
    icon: ICON,
    get label() {
        return pgettext("block:setter", "Set value");
    },
})
export class Setter extends NodeBlock {
    private advanced?: boolean;
    private purpose?: Forms.Radiobutton<boolean>;

    @definition
    @affects("#name")
    readonly values = Collection.of<Value, Setter>(Value, this);

    get isAdvanced(): boolean {
        return this.purpose
            ? this.purpose.value || false
            : isBoolean(this.advanced)
            ? this.advanced
            : this.verifyPurpose();
    }

    get label() {
        return npgettext(
            "block:setter",
            "%2 %1 value",
            "%2 %1 values",
            this.values.count,
            this.isAdvanced
                ? pgettext("block:setter", "Set")
                : pgettext("block:setter", "Prefill")
        );
    }

    private generateName(): void {
        if (this.node.nameVisible !== false) {
            this.node.name = Str.iterateToString(
                this.values.all,
                " ",
                (value) => "@" + value.variable
            );
        }
    }

    verifyPurpose(): boolean {
        const advanced = findFirst(this.values.all, (value) => {
            if (
                isBoolean(value.lock) ||
                isFilledString(value.mode) ||
                value.value === undefined
            ) {
                return true;
            }

            const variable =
                value.variable && lookupVariable(this, value.variable);

            return (
                (variable &&
                    value.value ===
                        (variable.slot instanceof Slots.Boolean ? 0 : false)) ||
                false
            );
        })
            ? true
            : false;

        if (this.purpose) {
            const current = this.purpose.buttonDisabled(false);

            if (current !== advanced) {
                this.purpose.buttonDisabled(false, advanced);
            }
        }

        this.advanced =
            this.values.all.length > 0 || isBoolean(this.advanced)
                ? advanced
                : undefined;

        return advanced;
    }

    @editor
    defineEditor(): void {
        let isAdvanced = this.isAdvanced;

        this.editor.form({
            title: pgettext("block:setter", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:setter",
                        "With this block you can change, lock or unlock values of (block) variables."
                    )
                ).markdown(),
            ],
        });

        this.purpose = new Forms.Radiobutton<boolean>(
            [
                {
                    label: pgettext("block:setter", "Prefilling"),
                    description: pgettext(
                        "block:setter",
                        "Prefill blocks with initial values."
                    ),
                    value: false,
                    disabled: isAdvanced,
                },
                {
                    label: pgettext("block:setter", "Advanced"),
                    description: pgettext(
                        "block:setter",
                        "Use options like value locking, write modes and more."
                    ),
                    value: true,
                },
            ],
            isAdvanced
        ).on(() => {
            if (isAdvanced !== this.purpose?.value) {
                isAdvanced = !isAdvanced;

                this.rerender();

                values.refresh();
            }
        });

        this.editor.option({
            name: pgettext("block:setter", "Purpose"),
            form: {
                title: pgettext("block:setter", "Purpose"),
                controls: [this.purpose],
            },
            activated: true,
            locked: true,
        });

        const values = this.editor.option({
            name: pgettext("block:setter", "Values"),
            collection: {
                collection: this.values,
                title: pgettext("block:setter", "Values"),
                icon: true,
                allowAutoSorting: false,
                allowCleanup: pgettext(
                    "block:setter",
                    "Remove invalid variables"
                ),
                placeholder: pgettext("block:setter", "Invalid variable"),
                allowVariables: true,
                allowFormatting: true,
                showAliases: true,
                markdown:
                    Markdown.MarkdownFeatures.Formatting |
                    Markdown.MarkdownFeatures.InlineCode,
                indicator: (value) =>
                    (isBoolean(value.lock) &&
                        (value.lock
                            ? pgettext("block:setter", "Lock")
                            : pgettext("block:setter", "Unlock")
                        ).toUpperCase()) ||
                    undefined,
                menu: () => variablesMenu(this, this.values, isAdvanced),
                onResize: () => {
                    if (this.values.count === 0) {
                        this.verifyPurpose();
                    }

                    this.generateName();
                },
                onReposition: () => this.generateName(),
                emptyMessage: {
                    message:
                        "**" +
                        pgettext(
                            "block:setter",
                            "Here you can specify the values you want to set."
                        ) +
                        "**\n\n" +
                        pgettext(
                            "block:setter",
                            "Click the + button to select a block or variable."
                        ),
                    height: 96,
                },
            },
            activated: true,
            locked: true,
        }).card as Collection.Card<Value, Setter>;

        this.editor.option({
            name: pgettext("block:setter", "Description"),
            form: {
                title: pgettext("block:setter", "Description"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        (!this.node.nameVisible && this.node.name) || ""
                    ).on((description) => {
                        this.node.nameVisible = !description.isFeatureEnabled;

                        if (!this.node.nameVisible) {
                            this.node.name = description.value;
                        } else {
                            this.generateName();
                        }
                    }),
                ],
            },
            activated: this.node.nameVisible === false,
        });

        this.editor.groups.options();
        this.editor.visibility();
    }
}
