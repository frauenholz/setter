/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { mountNamespace } from "tripetto";

mountNamespace(PACKAGE_NAME);
