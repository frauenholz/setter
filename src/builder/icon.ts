/** Dependencies */
import { Slots } from "tripetto";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_BOOLEAN from "../../assets/boolean.svg";
import ICON_DATE from "../../assets/date.svg";
import ICON_NUMERIC from "../../assets/numeric.svg";
import ICON_TEXT from "../../assets/text.svg";
import ICON_CALCULATOR from "../../assets/calculator.svg";
import ICON_SCORE from "../../assets/score.svg";

export function getIcon(slot: Slots.Slot | undefined) {
    if (slot instanceof Slots.Number || slot instanceof Slots.Numeric) {
        switch (slot.reference) {
            case "calculator":
                return ICON_CALCULATOR;
            case "score":
                return ICON_SCORE;
            default:
                return ICON_NUMERIC;
        }
    }

    if (slot instanceof Slots.Boolean) {
        return ICON_BOOLEAN;
    }

    if (slot instanceof Slots.Date) {
        return ICON_DATE;
    }

    if (slot instanceof Slots.String || slot instanceof Slots.Text) {
        return ICON_TEXT;
    }

    return ICON;
}
