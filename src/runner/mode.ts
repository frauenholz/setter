export type TMode =
    | "overwrite"
    | "concatenate"
    | "+"
    | "-"
    | "*"
    | "/"
    | "OR"
    | "NOR"
    | "AND"
    | "NAND"
    | "XOR"
    | "XNOR";
