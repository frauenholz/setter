/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    DateTime,
    HeadlessBlock,
    Slots,
    each,
    isBoolean,
    isDefined,
    isFilledString,
    isNumberFinite,
    isString,
    isVariable,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { IOperation, calculator } from "tripetto-block-calculator/runner";
import "./namespace/unmount";

@tripetto({
    type: "headless",
    identifier: PACKAGE_NAME,
})
export class Setter extends HeadlessBlock<{
    readonly values: {
        readonly variable?: string;
        readonly value?: string | number | boolean;
        readonly reference?: string;
        readonly querystring?: true;
        readonly lock?: boolean;
        readonly mode?: TMode;
        readonly operations?: IOperation[];
        readonly separator?: string;
    }[];
}> {
    private querystring(param: string, fallback?: string): string | undefined {
        const value = new URLSearchParams(
            document.location.search.substring(1)
        ).get(param);

        return isString(value) ? value : fallback;
    }

    do() {
        each(this.props.values, (value) => {
            if (
                value.variable &&
                (isDefined(value.value) || isBoolean(value.lock))
            ) {
                const variable = this.mutableValueOf(value.variable);

                if (variable && !variable.slot.protected) {
                    const isLocked = variable.isLocked;

                    if (isDefined(value.value)) {
                        if (isLocked) {
                            variable.unlock();
                        }

                        if (
                            value.value ===
                            (variable.slot instanceof Slots.Boolean ? 0 : false)
                        ) {
                            variable.clear();
                        } else if (
                            value.mode !== undefined ||
                            variable.allowPrefill
                        ) {
                            if (
                                variable.slot instanceof Slots.String ||
                                variable.slot instanceof Slots.Text
                            ) {
                                if (value.querystring) {
                                    const s = variable.slot.toValue(
                                        this.querystring(
                                            (isString(value.value) &&
                                                value.value) ||
                                                ""
                                        )
                                    );
                                    if (value.mode === "concatenate") {
                                        variable.concatenate(
                                            s,
                                            value,
                                            value.separator
                                        );
                                    } else {
                                        variable.set(s);
                                    }
                                } else if (
                                    isFilledString(value.value) &&
                                    isVariable(value.value)
                                ) {
                                    const input = this.variableFor(value.value);

                                    if (input && input.hasValue) {
                                        if (value.mode === "concatenate") {
                                            variable.concatenate(
                                                input.string,
                                                value,
                                                value.separator
                                            );
                                        } else {
                                            variable.set(
                                                input.string,
                                                input.reference
                                            );
                                        }
                                    } else if (value.mode !== "concatenate") {
                                        variable.clear();
                                    }
                                } else if (isString(value.value)) {
                                    const s = this.parseVariables(
                                        value.value,
                                        "",
                                        true
                                    );

                                    if (value.mode === "concatenate") {
                                        variable.concatenate(
                                            s,
                                            value,
                                            value.separator
                                        );
                                    } else {
                                        variable.set(s, value.reference);
                                    }
                                }
                            } else if (
                                variable.slot instanceof Slots.Number ||
                                variable.slot instanceof Slots.Numeric
                            ) {
                                const setValue = (n: number | undefined) => {
                                    if (isNumberFinite(n)) {
                                        const current =
                                            variable.hasValue &&
                                            (variable.slot instanceof
                                                Slots.Number ||
                                                variable.slot instanceof
                                                    Slots.Numeric)
                                                ? variable.slot.toValue(
                                                      variable.value
                                                  )
                                                : 0;

                                        switch (value.mode) {
                                            case "+":
                                                variable.operation(
                                                    current + n,
                                                    value
                                                );
                                                break;
                                            case "-":
                                                variable.operation(
                                                    current - n,
                                                    value
                                                );
                                                break;
                                            case "*":
                                                variable.operation(
                                                    current * n,
                                                    value
                                                );
                                                break;
                                            case "/":
                                                variable.operation(
                                                    n !== 0
                                                        ? current / n
                                                        : undefined,
                                                    value
                                                );
                                                break;
                                            default:
                                                variable.set(n);
                                                break;
                                        }
                                    } else if (
                                        value.mode !== "+" &&
                                        value.mode !== "-"
                                    ) {
                                        variable.clear();
                                    }
                                };

                                if (value.querystring) {
                                    setValue(
                                        variable.slot.toValue(
                                            this.querystring(
                                                (isString(value.value) &&
                                                    value.value) ||
                                                    ""
                                            )
                                        )
                                    );
                                } else if (value.value === true) {
                                    setValue(
                                        calculator(
                                            this.context,
                                            value.operations || [],
                                            undefined,
                                            undefined,
                                            (id) => this.variableFor(id),
                                            (id) => this.parseVariables(id)
                                        )
                                    );
                                } else if (isString(value.value)) {
                                    const input = this.variableFor(value.value);

                                    setValue(
                                        input &&
                                            (input.slot instanceof
                                                Slots.Number ||
                                                input.slot instanceof
                                                    Slots.Numeric) &&
                                            input.hasValue
                                            ? input.slot.toValue(input.value)
                                            : undefined
                                    );
                                } else if (isNumberFinite(value.value)) {
                                    setValue(value.value);
                                }
                            } else if (variable.slot instanceof Slots.Boolean) {
                                const setValue = (b: boolean | undefined) => {
                                    const current =
                                        variable.hasValue &&
                                        variable.slot instanceof Slots.Boolean
                                            ? variable.slot.toValue(
                                                  variable.value
                                              )
                                            : undefined;

                                    switch (value.mode) {
                                        case "OR":
                                            variable.operation(
                                                current || b ? true : false,
                                                value
                                            );
                                            break;
                                        case "NOR":
                                            variable.operation(
                                                current !== true && b !== true,
                                                value
                                            );
                                            break;
                                        case "AND":
                                            variable.operation(
                                                current === true && b === true,
                                                value
                                            );
                                            break;
                                        case "NAND":
                                            variable.operation(
                                                current !== true || b !== true,
                                                value
                                            );
                                            break;
                                        case "XOR":
                                            variable.operation(
                                                current !== b,
                                                value
                                            );
                                            break;
                                        case "XNOR":
                                            variable.operation(
                                                current === b,
                                                value
                                            );
                                            break;
                                        default:
                                            variable.set(b);
                                            break;
                                    }
                                };

                                if (value.querystring) {
                                    setValue(
                                        variable.slot.toValue(
                                            this.querystring(
                                                (isString(value.value) &&
                                                    value.value) ||
                                                    "",
                                                "false"
                                            )
                                        )
                                    );
                                } else if (isString(value.value)) {
                                    const input = this.variableFor(value.value);

                                    setValue(
                                        input &&
                                            input.slot instanceof
                                                Slots.Boolean &&
                                            input.hasValue
                                            ? input.slot.toValue(input.value)
                                            : undefined
                                    );
                                } else if (isBoolean(value.value)) {
                                    setValue(value.value);
                                }
                            } else if (variable.slot instanceof Slots.Date) {
                                if (value.querystring) {
                                    variable.set(
                                        variable.slot.toValue(
                                            this.querystring(
                                                (isString(value.value) &&
                                                    value.value) ||
                                                    ""
                                            )
                                        )
                                    );
                                } else if (value.value === true) {
                                    variable.set(DateTime.UTC);
                                } else if (isString(value.value)) {
                                    const input = this.variableFor(value.value);

                                    variable.set(
                                        input &&
                                            input.slot instanceof Slots.Date &&
                                            input.hasValue
                                            ? input.slot.toValue(input.value)
                                            : undefined
                                    );
                                } else if (isNumberFinite(value.value)) {
                                    variable.set(value.value);
                                }
                            }
                        }
                    }

                    if (isBoolean(value.lock) || isLocked) {
                        if (!isBoolean(value.lock) || value.lock) {
                            variable.lock();
                        } else {
                            variable.unlock();
                        }
                    }
                }
            }
        });
    }
}
